# steamclient

## Game
```python
Game(self, user_id, id, name)
```

### grid
Returns path to cover art on disk (might not exist)
### hero
Returns path to banner on disk (might not exist)
### logo
Returns path to logo on disk (might not exist)
### set_logo
```python
Game.set_logo(self, filepath=None, url=None)
```

Copies an image to the custom artwork directory.
Image can be either a file or a URL.

:param filepath: File on disk
:param url: URL of image.

### set_grid
```python
Game.set_grid(self, filepath=None, url=None)
```

Copies an image to the custom artwork directory.
Image can be either a file or a URL.

:param filepath: File on disk
:param url: URL of image.

### set_hero
```python
Game.set_hero(self, filepath=None, url=None)
```

Copies an image to the custom artwork directory.
Image can be either a file or a URL.

:param filepath: File on disk
:param url: URL of image.

## User
```python
User(self, id)
```
User object to store the ID
### shortcuts
Returns a list of Shortcut objects
### games
```python
User.games(self, libraries=None)
```
Returns a list of Shortcut objects
## get_games
```python
get_games(user_id, libraries=None)
```

Get all of a user's installed games

:param user_id: the user's ID number
:param libraries: list of file paths, if None is given - they are all loaded
:returns: a list of Game objects

## get_libraries
```python
get_libraries()
```

Get a list of paths to all Steam Libraries from
Steam\steamapps\libraryfolders.vdf

:returns: a list of file paths

## get_shortcuts
```python
get_shortcuts(user_id)
```

Get a user's non-steam Shortcuts from their shortcuts.vdf

:param user_id: the user's ID number
:returns: a list of Shortcut objects

## get_users
```python
get_users()
```

Get all users that have logged in.

:returns: a list of User objects (sorted by last login date)

# Shortcut
```python
Shortcut(self, user_id, entry_id, name, exe, start_dir, icon='', shortcut_path='', launch_options='', hidden=False, allow_desktop_config=True, allow_overlay=True, openvr=False, devkit=False, devkit_game_id='', last_play_time=b'\x00\x00\x00\x00', tags=[])
```

