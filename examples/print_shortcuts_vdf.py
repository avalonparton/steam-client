""" Prints the contents of shortcuts.vdf (for debugging) """

import steamclient

user = steamclient.get_users()[0]
path = f'{steamclient.STEAM_PATH}\\userdata\\{user.id}\\config\\shortcuts.vdf'

with open(path, "rb") as f:
    data = f.read()
    print("----------------------")
    print(f"Contents of {path}")
    print(data)
    print("----------------------")
    print("List of Shortcuts:")
    for n, shortcut in enumerate(data.split(b"AppName\x00")[1:]):
        print(f"Shortcut {n}")
        print(f"   {shortcut}")